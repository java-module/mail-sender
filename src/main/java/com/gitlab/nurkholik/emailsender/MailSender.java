package com.gitlab.nurkholik.emailsender;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class MailSender {
	
	private String auth;
	private String enableStartTLS;
	private String host;
	private String port;
	
	private String address;
	private String password;
	private String name;
	
	private Session SESSION;
	private String bodyTemplate;
	private InternetAddress senderAddress;
	
	private final String HTML_TYPE 			= "text/html";
	private final String PLAIN_TYPE 		= "text/plain";
	private final String DEFAULT_HTML_BODY 	= "<html><head></head><body><p>%s</p></body></html>";
	
	public MailSender(String auth, String enableStartTLS, String host, String port, String address, String password,
			String name, String bodyTemplate) throws AddressException {
		super();
		if (bodyTemplate == null) bodyTemplate = "";
		
		this.auth = auth;
		this.enableStartTLS = enableStartTLS;
		this.host = host;
		this.port = port;
		this.address = address;
		this.password = password;
		this.name = name;
		this.bodyTemplate = !(bodyTemplate.equals("") || bodyTemplate.equals("-")) ? bodyTemplate : DEFAULT_HTML_BODY;
		
		initSession();
		initSenderAddress();
	}

	private void initSession() {
		System.out.println("Initialize mail service session ...");
		Properties props = new Properties();
		props.put("mail.smtp.auth", 			this.auth);
		props.put("mail.smtp.starttls.enable", 	this.enableStartTLS);
		props.put("mail.smtp.host", 			this.host);
		props.put("mail.smtp.port", 			this.port);
		
		SESSION	= Session.getDefaultInstance(props, 
		new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication(){
				return new PasswordAuthentication(address, password);
			}
		});
		System.out.println("Session initialized successfully.");
	}
	
	private void initSenderAddress() throws AddressException {
		try {
			senderAddress = new InternetAddress(this.address, this.name);
		} catch (UnsupportedEncodingException e1) {
			senderAddress = new InternetAddress(this.address);
			e1.printStackTrace();
		}
	}
	
	
	// PLAIN Model
	public void sendPlain(String LOG_TAG, String mail_subject, String mail_message, String recipients, File...attach) throws AddressException, MessagingException {
		send(LOG_TAG, mail_subject, mail_message, recipients, PLAIN_TYPE, attach);
	}
	public void sendPlain(String LOG_TAG, String mail_subject, String mail_message, List<String> recipients, File...attach) throws AddressException, MessagingException {
		send(LOG_TAG, mail_subject, mail_message, recipients.toArray(new String[recipients.size()]), PLAIN_TYPE, attach);
	}
	public void sendPlain(String LOG_TAG, String mail_subject, String mail_message, String[] recipients, File...attach) throws AddressException, MessagingException {
		send(LOG_TAG, mail_subject, mail_message, recipients, PLAIN_TYPE, attach);
	}
	
	
	// HTML Model
	public void sendHtml(String LOG_TAG, String mail_subject, String mail_message, String recipients, File...attach) throws AddressException, MessagingException {
		send(LOG_TAG, mail_subject, mail_message, recipients, HTML_TYPE, attach);
	}
	public void sendHtml(String LOG_TAG, String mail_subject, String mail_message, List<String> recipients, File...attach) throws AddressException, MessagingException {
		send(LOG_TAG, mail_subject, mail_message, recipients.toArray(new String[recipients.size()]), HTML_TYPE, attach);
	}
	public void sendHtml(String LOG_TAG, String mail_subject, String mail_message, String[] recipients, File...attach) throws AddressException, MessagingException {
		send(LOG_TAG, mail_subject, mail_message, recipients, HTML_TYPE, attach);
	}
	
	/**
	 * ------------------------------------------------------
	 * | Sending email										|
	 * | for attachment, send parameter using array list.	|
	 * | if not using attachment, send null parameter		|
	 * ------------------------------------------------------
	 * @param mail_subject			-> Mail Subject
	 * @param mail_message			-> Message Content
	 * @param recipients			-> Email, use semicolon (;) to separate between recipients
	 * @param attach				-> List Attachment File
	 * @param message_type			-> Type of message, use constant variable on this class
	 * @throws AddressException
	 * @throws MessagingException
	 */
	private void send(String LOG_TAG, String mail_subject, String mail_message, String recipients, String message_type , File...attach) throws AddressException, MessagingException {
		send(LOG_TAG, mail_subject, mail_message, recipients.split("[;]"), message_type, attach);
	}
	
	private void send(String LOG_TAG, String mail_subject, String mail_message, String[] recipients, String message_type , File...attach) throws AddressException, MessagingException {
		
		System.out.println("Preparing ... ");
		if (message_type.equals(HTML_TYPE)){
			mail_message = bodyTemplate.replace("%s", mail_message);
		}
		
		System.out.println("Create email data ...");
		MimeMessage message = new MimeMessage(SESSION);
		message.setFrom(senderAddress);
		message.setSubject(mail_subject);
		for (String recipient : recipients) {
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(recipient.trim()));
		}
		
		// Main Message
		Multipart multipart = new MimeMultipart();
		
		// Body Message
		MimeBodyPart body = new MimeBodyPart();
		body.setContent(mail_message, message_type);
		multipart.addBodyPart(body);
		
		// Attachment
		System.out.println("Attach file ...");
		for (File atFile : attach) {
			MimeBodyPart attachment= new MimeBodyPart();
			try {
				attachment.attachFile(atFile);
			} catch (IOException e) {
				e.printStackTrace();
			}
			multipart.addBodyPart(attachment);
		}
		
		// Set Content
		System.out.println("Sending ...");
		message.setContent(multipart);
		Transport.send(message);
		System.out.println("Sending email finished.");
	}
	
}
